import os
import ray
import tqdm
import argparse
import ujson as json
from pathlib import Path
from verbalized_envs import VerbalizedALFWorld


@ray.remote
def generate_rollout(gamefile, dout, max_steps, force=False):
    game = '/'.join(gamefile.split('/')[-3:-1])
    fout = os.path.join(dout, '{}.json'.format(game.replace('/', '__')))

    if os.path.isfile(fout) and not force:
        return

    env = VerbalizedALFWorld('train', force_game=game, load_expert=True, return_history=True, max_steps=max_steps)

    obs, info = env.reset()
    done = False
    while not done:
        obs, reward, done, truncated, info = env.step(info['expert'])
    data = dict(
        game=game,
        task=info['task'],
        history=info['history'],
    )
    with open(fout, 'wt') as f:
        json.dump(data, f, indent=2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dout', default='data/bc')
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--num_procs', default=1, type=int)
    parser.add_argument('--max_steps', default=150, type=int)
    args = parser.parse_args()

    dout = Path(args.dout)
    if not dout.exists():
        os.makedirs(dout)

    kwargs = dict()
    if args.num_procs == 1:
        kwargs['local_mode'] = True
    ray.init(**kwargs)

    env = VerbalizedALFWorld('train')
    gamefiles = env.gamefiles
    env.close()

    object_references = [generate_rollout.remote(gamefile, dout, max_steps=args.max_steps, force=args.regenerate) for gamefile in gamefiles]
    bar = tqdm.tqdm(total=len(object_references))
    while object_references:
        finished, object_references = ray.wait(object_references, timeout=120)
        data = ray.get(finished)  # this returns whatever the remote function returns, which is our case is None
        bar.update(len(data))
    bar.close()
