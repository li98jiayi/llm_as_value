# docs and experiment results can be found at https://docs.cleanrl.dev/rl-algorithms/ppo/#ppopy
import os
import random
import time
from dataclasses import dataclass

import gymnasium as gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import tyro
from torch.distributions.categorical import Categorical
from torch.utils.tensorboard import SummaryWriter
from collections import defaultdict

import typing as T
from tqdm import auto as tqdm
from verbalized_envs import VerbalizedALFWorld
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


class Buffer:

    def __init__(self, buffer_size):
        self.buffer_size = buffer_size
        self.cache = defaultdict(lambda: [None] * buffer_size)

    def set(self, i, key, value):
        assert 0 <= i < self.buffer_size
        self.cache[key][i] = value

    def get(self, i, key):
        assert 0 <= i < self.buffer_size
        return self.cache[key][i]

    def get_all(self, key, merge='stack'):
        x = self.cache[key]
        if merge == 'none':
            return x
        elif merge == 'stack':
            return torch.stack(x, dim=0)
        elif merge == 'flatten':
            out = []
            for xi in x:
                out.extend(list(xi))
            return out
        else:
            raise NotImplementedError('Unsupported merge "{}"'.format(merge))


@dataclass
class Args:
    exp_name: str = os.path.basename(__file__)[: -len(".py")]
    """the name of this experiment"""
    seed: int = 1
    """seed of the experiment"""
    torch_deterministic: bool = True
    """if toggled, `torch.backends.cudnn.deterministic=False`"""
    cuda: bool = True
    """if toggled, cuda will be enabled by default"""
    track: bool = False
    """if toggled, this experiment will be tracked with Weights and Biases"""
    wandb_project_name: str = "r2l-tutorial-planning"
    """the wandb's project name"""
    wandb_entity: str = None
    """the entity (team) of wandb's project"""

    # Algorithm specific arguments
    # env_id: str = "CartPole-v1"
    # """the id of the environment"""
    total_timesteps: int = 1000000
    """total timesteps of the experiments"""
    learning_rate: float = 2.5e-4
    """the learning rate of the optimizer"""
    num_envs: int = 4
    """the number of parallel game environments"""
    num_steps: int = 80
    """the number of steps to run in each environment per policy rollout"""
    anneal_lr: bool = False
    """Toggle learning rate annealing for policy and value networks"""
    gamma: float = 0.99
    """the discount factor gamma"""
    gae_lambda: float = 0.95
    """the lambda for the general advantage estimation"""
    num_minibatches: int = 8
    """the number of mini-batches"""
    update_epochs: int = 4
    """the K epochs to update the policy"""
    norm_adv: bool = True
    """Toggles advantages normalization"""
    clip_coef: float = 0.2
    """the surrogate clipping coefficient"""
    clip_vloss: bool = True
    """Toggles whether or not to use a clipped loss for the value function, as per the paper."""
    ent_coef: float = 0.01
    """coefficient of the entropy"""
    vf_coef: float = 0.5
    """coefficient of the value function"""
    max_grad_norm: float = 0.5
    """the maximum norm for the gradient clipping"""
    target_kl: float = None
    """the target KL divergence threshold"""

    # to be filled in runtime
    batch_size: int = 0
    """the batch size (computed in runtime)"""
    minibatch_size: int = 0
    """the mini-batch size (computed in runtime)"""
    num_iterations: int = 0
    """the number of iterations (computed in runtime)"""

    # custom
    split: str = 'train'
    """environment split"""
    alfworld_data: str = os.path.join(os.environ['HOME'], '.alfworld')
    """alfworld data root"""
    base_model_id: str = 'google/flan-t5-small'
    """base LLM to use"""
    max_input_len: int = 512
    """max number of tokens in input"""
    num_actions: int = 100
    """max number of actions"""
    progress_bar: bool = False
    """show progress bar"""
    save_every_iterations: int = 100
    """how many interations to save"""
    dout: str = 'results/ppo'
    """where to save results"""
    fpretrained: str = None
    """load pretrained BC model"""


def make_env(args):

    def thunk():
        return VerbalizedALFWorld(split=args.split, max_steps=args.num_steps, shuffle=True, alfworld_data=args.alfworld_data)

    return thunk


def layer_init(layer, std=np.sqrt(2), bias_const=0.0):
    torch.nn.init.orthogonal_(layer.weight, std)
    torch.nn.init.constant_(layer.bias, bias_const)
    return layer


class Agent(nn.Module):

    def __init__(self, args):
        super().__init__()
        self.args = args
        self.tokenizer = AutoTokenizer.from_pretrained(args.base_model_id)
        self.llm = AutoModelForSeq2SeqLM.from_pretrained(args.base_model_id)
        self.hidden_size = self.llm.config.hidden_size
        self.action_scorer = nn.Sequential(
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(),
            nn.Linear(self.hidden_size, 1),
        )
        self.value_scorer = nn.Sequential(
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(),
            nn.Linear(self.hidden_size, 1),
        )

    @property
    def device(self):
        return self.action_scorer[0].weight.device

    def encode(self, text: T.List[T.AnyStr]):
        context = self.tokenizer(text, max_length=self.args.max_input_len, padding=True, truncation=True, add_special_tokens=True, return_tensors='pt').to(self.device)
        encoder_output_vectors = self.llm.base_model.encoder(
            input_ids=context.input_ids, attention_mask=context.attention_mask, return_dict=True
        ).last_hidden_state
        return encoder_output_vectors

    def forward(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]], encoder_output_vectors=None, num_actions=None):
        if encoder_output_vectors is None:
            encoder_output_vectors = self.encode(text)
        admissible = admissible[:num_actions]
        max_admissible = max(len(a) for a in admissible)
        admissible_mask = []
        last_state = []
        for enc, a in zip(encoder_output_vectors, admissible):
            admissible_mask.append([1] * len(a) + [0] * (max_admissible - len(a)))
            a.extend(['nop'] * (max_admissible - len(a)))
            action = self.tokenizer(['{} {}'.format(self.tokenizer.pad_token_id, ai) for ai in a], padding=True, truncation=True, add_special_tokens=False, return_tensors='pt').to(self.device)
            enc = enc.unsqueeze(0).repeat(len(a), 1, 1)
            decoder_output_vectors = self.llm.base_model.decoder(
                action.input_ids, encoder_hidden_states=enc
            ).last_hidden_state
            last_state.append(decoder_output_vectors[:, -1])
        last_state = torch.stack(last_state, dim=0)
        valid = torch.tensor(admissible_mask, device=self.device, dtype=torch.float)
        return last_state, valid

    def get_value(self, text: T.List[T.AnyStr]):
        encoder_output_vectors = self.encode(text)
        return self.value_scorer(encoder_output_vectors[:, 0])  # use first position

    def get_action_and_value(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]], action=None):
        encoder_output_vectors = self.encode(text)
        value = self.value_scorer(encoder_output_vectors[:, 0])  # use first position
        decoder_last_state, valid = self.forward(text, admissible=admissible, encoder_output_vectors=encoder_output_vectors, num_actions=self.args.num_actions)
        logits = self.action_scorer(decoder_last_state).squeeze(2)
        logits = logits - 1e20 * (1-valid)
        probs = Categorical(logits=logits)
        if action is None:
            action = probs.sample()
        return action, probs.log_prob(action), probs.entropy(), value

    def act(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]], sample: bool = False, return_raw_ids: bool = False):
        decoder_last_state, valid = self.forward(text, admissible=admissible, num_actions=self.args.num_actions)
        logits = self.action_scorer(decoder_last_state).squeeze(2)
        logits = logits - 1e20 * (1-valid)
        ids = logits.max(1)[1]
        if sample:
            probs = Categorical(logits=value)
            ids = probs.sample()
        else:
            ids = logits.max(1)[1]
        return ids if return_raw_ids else [a[i] for a, i in zip(admissible, ids.tolist())]


if __name__ == "__main__":
    args = tyro.cli(Args)
    args.batch_size = int(args.num_envs * args.num_steps)
    args.minibatch_size = int(args.batch_size // args.num_minibatches)
    args.num_iterations = args.total_timesteps // args.batch_size
    # run_name = f"{args.env_id}__{args.exp_name}__{args.seed}__{int(time.time())}"
    run_name = f"alfworld-{args.split}__{args.exp_name}__{args.seed}"
    if args.track:
        import wandb

        wandb.init(
            project=args.wandb_project_name,
            entity=args.wandb_entity,
            sync_tensorboard=True,
            config=vars(args),
            name=run_name,
            monitor_gym=True,
            save_code=True,
        )
    dout = f"{args.dout}/{run_name}"
    writer = SummaryWriter(dout)
    writer.add_text(
        "hyperparameters",
        "|param|value|\n|-|-|\n%s" % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )

    # TRY NOT TO MODIFY: seeding
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")

    # env setup
    envs = gym.vector.SyncVectorEnv(
        [make_env(args) for i in range(args.num_envs)],
    )
    # assert isinstance(envs.single_action_space, gym.spaces.Discrete), "only discrete action space is supported"

    agent = Agent(args).to(device)
    optimizer = optim.Adam(agent.parameters(), lr=args.learning_rate, eps=1e-5)

    if args.fpretrained:
        state = torch.load(args.fpretrained)
        partial = {k.replace('agent.', ''): v for k, v in state['state_dict'].items() if k.startswith('agent.')}
        agent.load_state_dict(partial)

    # ALGO Logic: Storage setup
    # obs = torch.zeros((args.num_steps, args.num_envs) + envs.single_observation_space.shape).to(device)
    # actions = torch.zeros((args.num_steps, args.num_envs) + envs.single_action_space.shape).to(device)
    # logprobs = torch.zeros((args.num_steps, args.num_envs)).to(device)
    # rewards = torch.zeros((args.num_steps, args.num_envs)).to(device)
    # dones = torch.zeros((args.num_steps, args.num_envs)).to(device)
    # values = torch.zeros((args.num_steps, args.num_envs)).to(device)
    buffer = Buffer(args.num_steps)

    # TRY NOT TO MODIFY: start the game
    global_step = num_completions = 0
    start_time = time.time()
    next_obs, infos = envs.reset(seed=args.seed)
    next_admissible = infos['admissible']
    next_done = torch.zeros(args.num_envs).to(device)

    for iteration in tqdm.trange(1, args.num_iterations + 1, desc='iteration', disable=not args.progress_bar, position=0):
        if iteration % args.save_every_iterations == 0:
            torch.save(dict(
                agent=agent.state_dict(),
                optimizer=optimizer.state_dict(),
                args=args,
                global_step=global_step,
                num_completions=num_completions,
                iteration=iteration,
            ), os.path.join(dout, 'save_iter{}.ckpt'.format(iteration)))
        # Annealing the rate if instructed to do so.
        if args.anneal_lr:
            frac = 1.0 - (iteration - 1.0) / args.num_iterations
            lrnow = frac * args.learning_rate
            optimizer.param_groups[0]["lr"] = lrnow

        for step in tqdm.trange(0, args.num_steps, desc='rollout', disable=not args.progress_bar, position=1, leave=False):
            global_step += args.num_envs
            # obs[step] = next_obs
            buffer.set(step, 'obs', next_obs)
            buffer.set(step, 'admissible', next_admissible)
            # dones[step] = next_done
            buffer.set(step, 'done', next_done)

            # ALGO LOGIC: action logic
            with torch.no_grad():
                action, logprob, _, value = agent.get_action_and_value(next_obs, next_admissible)
                real_action = [a[i] for i, a in zip(action.tolist(), next_admissible)]
                # values[step] = value.flatten()
                buffer.set(step, 'value', value.flatten())
            # actions[step] = action
            buffer.set(step, 'action', action)
            # logprobs[step] = logprob
            buffer.set(step, 'logprob', logprob)

            # TRY NOT TO MODIFY: execute the game and log data.
            # next_obs, reward, terminations, truncations, infos = envs.step(action.cpu().numpy())
            next_obs, reward, terminations, truncations, infos = envs.step(real_action)
            next_admissible = infos['admissible']
            next_done = np.logical_or(terminations, truncations)
            # rewards[step] = torch.tensor(reward).to(device).view(-1)
            buffer.set(step, 'reward', torch.tensor(reward).to(device).view(-1))
            # next_obs, next_done = torch.Tensor(next_obs).to(device), torch.Tensor(next_done).to(device)
            next_done = torch.Tensor(next_done).to(device)

            if "final_info" in infos:
                for info in infos["final_info"]:
                    if info and "episode" in info:
                        num_completions += 1
                        print(f"global_step={global_step}, episodic_return={info['episode']['r']}")
                        writer.add_scalar("charts/num_completions", num_completions, global_step)
                        writer.add_scalar("charts/episodic_return", info["episode"]["r"], global_step)
                        writer.add_scalar("charts/episodic_length", info["episode"]["l"], global_step)

        # bootstrap value if not done
        with torch.no_grad():
            next_value = agent.get_value(next_obs).reshape(1, -1)
            # advantages = torch.zeros_like(rewards).to(device)
            advantages = torch.zeros_like(buffer.get_all('reward')).to(device)
            lastgaelam = 0
            for t in reversed(range(args.num_steps)):
                if t == args.num_steps - 1:
                    nextnonterminal = 1.0 - next_done
                    nextvalues = next_value
                else:
                    # nextnonterminal = 1.0 - dones[t + 1]
                    nextnonterminal = 1.0 - buffer.get(t + 1, 'done')
                    # nextvalues = values[t + 1]
                    nextvalues = buffer.get(t + 1, 'value')
                # delta = rewards[t] + args.gamma * nextvalues * nextnonterminal - values[t]
                delta = buffer.get(t, 'reward') + args.gamma * nextvalues * nextnonterminal - buffer.get(t, 'value')
                advantages[t] = lastgaelam = delta + args.gamma * args.gae_lambda * nextnonterminal * lastgaelam
            returns = advantages + buffer.get_all('value')

        # flatten the batch
        # b_obs = obs.reshape((-1,) + envs.single_observation_space.shape)
        b_obs = buffer.get_all('obs', merge='flatten')
        b_admissible = buffer.get_all('admissible', merge='flatten')
        b_logprobs = buffer.get_all('logprob').reshape(-1)
        # b_actions = buffer.get_all('action').reshape((-1,) + envs.single_action_space.shape)
        b_actions = buffer.get_all('action').reshape(-1)
        b_advantages = advantages.reshape(-1)
        b_returns = returns.reshape(-1)
        b_values = buffer.get_all('value').reshape(-1)

        # Optimizing the policy and value network
        b_inds = np.arange(args.batch_size)
        clipfracs = []
        for epoch in tqdm.trange(args.update_epochs, desc='update epoch', disable=not args.progress_bar, position=1, leave=False):
            np.random.shuffle(b_inds)
            for start in range(0, args.batch_size, args.minibatch_size):
                end = start + args.minibatch_size
                mb_inds = b_inds[start:end]

                # _, newlogprob, entropy, newvalue = agent.get_action_and_value(b_obs[mb_inds], b_actions.long()[mb_inds])
                # _, newlogprob, entropy, newvalue = agent.get_action_and_value(torch.stack([b_obs[i] for i in mb_inds]), b_actions.long()[mb_inds])
                _, newlogprob, entropy, newvalue = agent.get_action_and_value([b_obs[i] for i in mb_inds], [b_admissible[i] for i in mb_inds], b_actions.long()[mb_inds])
                logratio = newlogprob - b_logprobs[mb_inds]
                ratio = logratio.exp()

                with torch.no_grad():
                    # calculate approx_kl http://joschu.net/blog/kl-approx.html
                    old_approx_kl = (-logratio).mean()
                    approx_kl = ((ratio - 1) - logratio).mean()
                    clipfracs += [((ratio - 1.0).abs() > args.clip_coef).float().mean().item()]

                mb_advantages = b_advantages[mb_inds]
                if args.norm_adv:
                    mb_advantages = (mb_advantages - mb_advantages.mean()) / (mb_advantages.std() + 1e-8)

                # Policy loss
                pg_loss1 = -mb_advantages * ratio
                pg_loss2 = -mb_advantages * torch.clamp(ratio, 1 - args.clip_coef, 1 + args.clip_coef)
                pg_loss = torch.max(pg_loss1, pg_loss2).mean()

                # Value loss
                newvalue = newvalue.view(-1)
                if args.clip_vloss:
                    v_loss_unclipped = (newvalue - b_returns[mb_inds]) ** 2
                    v_clipped = b_values[mb_inds] + torch.clamp(
                        newvalue - b_values[mb_inds],
                        -args.clip_coef,
                        args.clip_coef,
                    )
                    v_loss_clipped = (v_clipped - b_returns[mb_inds]) ** 2
                    v_loss_max = torch.max(v_loss_unclipped, v_loss_clipped)
                    v_loss = 0.5 * v_loss_max.mean()
                else:
                    v_loss = 0.5 * ((newvalue - b_returns[mb_inds]) ** 2).mean()

                entropy_loss = entropy.mean()
                loss = pg_loss - args.ent_coef * entropy_loss + v_loss * args.vf_coef

                optimizer.zero_grad()
                loss.backward()
                nn.utils.clip_grad_norm_(agent.parameters(), args.max_grad_norm)
                optimizer.step()

            if args.target_kl is not None and approx_kl > args.target_kl:
                break

        y_pred, y_true = b_values.cpu().numpy(), b_returns.cpu().numpy()
        var_y = np.var(y_true)
        explained_var = np.nan if var_y == 0 else 1 - np.var(y_true - y_pred) / var_y

        # TRY NOT TO MODIFY: record rewards for plotting purposes
        writer.add_scalar("charts/learning_rate", optimizer.param_groups[0]["lr"], global_step)
        writer.add_scalar("losses/value_loss", v_loss.item(), global_step)
        writer.add_scalar("losses/policy_loss", pg_loss.item(), global_step)
        writer.add_scalar("losses/entropy", entropy_loss.item(), global_step)
        writer.add_scalar("losses/old_approx_kl", old_approx_kl.item(), global_step)
        writer.add_scalar("losses/approx_kl", approx_kl.item(), global_step)
        writer.add_scalar("losses/clipfrac", np.mean(clipfracs), global_step)
        writer.add_scalar("losses/explained_variance", explained_var, global_step)
        print("SPS:", int(global_step / (time.time() - start_time)))
        writer.add_scalar("charts/SPS", int(global_step / (time.time() - start_time)), global_step)

    envs.close()
    writer.close()
