import os
import yaml
import string
import requests
import textworld
import contextlib
import gymnasium as gym
from pathlib import Path
from gymnasium import spaces as S
from alfworld.agents.environment.alfred_tw_env import AlfredTWEnv, AlfredInfos, AlfredDemangler, AlfredExpert


class VerbalizedEnv(gym.Env):

    metadata = dict(render_mode=['ascii'])

    def __init__(self, max_action_len=40, max_obs_len=10000, return_history=False, context_len=5):
        super().__init__()
        self.action_space = S.Text(min_length=1, max_length=max_action_len, charset=string.ascii_letters)
        self.observation_space = S.Text(min_length=1, max_length=max_obs_len, charset=string.ascii_letters)
        self.reward_range = 0, 1
        self.history = []
        self.return_history = return_history
        self.task = None
        self.context_len = context_len

    def my_reset(self, seed=None, options=None):
        raise NotImplementedError()

    def my_step(self, action):
        raise NotImplementedError()

    def align_action(self, action):
        return action

    def get_admissible(self):
        return self.history[-1]['info']['admissible']

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        task, obs, info = self.my_reset(seed=seed, options=options)
        self.task = task
        self.history.clear()
        self.history.append(dict(
            obs=obs, reward=False, done=False, truncated=False,
            info={k: v for k, v in info.items() if k != 'history'},
        ))
        info['task'] = task
        if self.return_history:
            info['history'] = self.history.copy()
        return self.render(context_len=self.context_len), info

    def step(self, action):
        assert self.history, 'Missing history when attempting action "{}"'.format(action)
        aligned = self.align_action(action)
        self.history[-1]['act'] = dict(raw=action, taken=aligned)
        obs, reward, done, truncated, info = self.my_step(aligned)
        self.history.append(dict(
            obs=obs, reward=reward, done=done, truncated=truncated,
            info={k: v for k, v in info.items() if k != 'history'},
        ))
        info['task'] = self.task
        if self.return_history:
            info['history'] = self.history.copy()
        if done:
            info['episode'] = dict(r=sum(s['reward'] for s in self.history), l=len(self.history))
        return self.render(context_len=self.context_len), reward, done, truncated, info

    @classmethod
    def render_action_prompt(cls, step):
        context = ['You choose:\n']
        return context

    @classmethod
    def render_indenpendent(cls, task, history, context_len=10):
        context = ['Your task is: {}'.format(task)]
        for t in range(max(0, len(history) - context_len), len(history)):
            step = history[t]
            context.append('Step t-{}'.format(len(history) - t - 1))
            context.append('You see:\n{}'.format(step['obs']))
            if step != history[-1]:
                context.append('You choose:\n{}'.format(step['act']['taken']))
            else:
                context.extend(cls.render_action_prompt(step))
        return '\n'.join(context)

    def render(self, context_len=5):
        return self.render_indenpendent(self.task, self.history, context_len=context_len)


class AlfredTWEnvWrapper(AlfredTWEnv):

    def init_env(self, batch_size, expert_plan=False, shuffle=False, max_steps=50):
        alfred_demangler = AlfredDemangler(shuffle=shuffle)
        wrappers = [alfred_demangler, AlfredInfos]
        # Register a new Gym environment.
        request_infos = textworld.EnvInfos(won=True, admissible_commands=True, extras=["gamefile"])
        expert_type = self.config["env"]["expert_type"]
        if expert_plan:
            wrappers.append(AlfredExpert(expert_type))
            request_infos.extras.append("expert_plan")
        env_id = textworld.gym.register_games(self.game_files, request_infos,
                                              batch_size=batch_size,
                                              asynchronous=True,
                                              max_episode_steps=max_steps,
                                              wrappers=wrappers)
        env = textworld.gym.make(env_id)
        return env


class VerbalizedALFWorld(VerbalizedEnv):

    def __init__(self, split, max_steps=40, force_game=None, load_expert=False, shuffle=True, config=None, verbose=False, alfworld_data=os.path.join(os.environ['HOME'], '.alfworld'), *args, **kwargs):
        super().__init__(*args, **kwargs)

        os.environ['ALFWORLD_DATA'] = alfworld_data
        if config is None:
            fconfig = os.path.join(alfworld_data, 'default_config.yaml')
            config = self.get_alfworld_config(fconfig, max_training_steps=max_steps, load_expert=load_expert)

        if force_game is not None:
            for k in ['data_path', 'eval_id_data_path', 'eval_ood_data_path']:
                config['dataset'][k] = os.path.join(config['dataset'][k], force_game)

        init_kwargs = dict(
            batch_size=1,
            expert_plan=load_expert,
            shuffle=shuffle,
            max_steps=max_steps,
        )

        if verbose:
            self.raw_env = AlfredTWEnvWrapper(config, train_eval=split)
            self.env = self.raw_env.init_env(**init_kwargs)
        else:
            with open(os.devnull, 'w') as null:
                with contextlib.redirect_stderr(null):
                    with contextlib.redirect_stdout(null):
                        self.raw_env = AlfredTWEnvWrapper(config, train_eval=split)
                        self.env = self.raw_env.init_env(**init_kwargs)
        self.gamefiles = self.env.gamefiles

    def close(self):
        self.env.close()
        try:
            self.raw_env.close()
        except Exception:
            pass

    def extract_info(self, infos):
        out = dict(admissible=infos['admissible_commands'][0], game=infos['extra.gamefile'][0])
        if 'extra.expert_plan' in infos:
            out['expert'] = infos['extra.expert_plan'][0][0]
        return out

    def my_reset(self, seed=None, options=None):
        self.env.seed(seed)
        obs, infos = self.env.reset()
        task, o = self.extract_initial_obs(obs[0])
        return task, o, self.extract_info(infos)

    def my_step(self, action):
        obs, scores, dones, infos = self.env.step([action])
        won = infos['won'][0]
        done = dones[0]
        reward = 1 if won else 0
        truncated = done and not won
        return obs[0], reward, done, truncated, self.extract_info(infos)

    @classmethod
    def extract_initial_obs(cls, obs):
        lines = obs.split('\n')
        o = lines[2]
        task = lines[-1].replace('..', '.').replace('Your task is to: ', '')
        return task, o

    @classmethod
    def get_alfworld_config(cls, fout, load_expert=False, max_training_steps=40):
        parent = os.path.dirname(fout)
        if not os.path.isdir(parent):
            os.makedirs(parent)
        if os.path.isfile(fout):
            with open(fout) as f:
                content = f.read()
        else:
            config_url = 'https://raw.githubusercontent.com/alfworld/alfworld/master/configs/base_config.yaml'
            response = requests.get(config_url)
            content = response.content
            with open(fout, 'wt') as f:
                f.write(content.decode())
        config = yaml.safe_load(content)
        root = Path(os.environ['ALFWORLD_DATA'])
        if not root.joinpath('synth').exists():
            raise Exception("""
Please run the following command to generate data:
ALFWORLD_DATA=$HOME/.alfworld alfworld-generate --data_path $ALFWORLD_DATA/json_2.1.1 --save_path $ALFWORLD_DATA/synth --goal_desc_human_anns_prob 0
ALFWORLD_DATA=$HOME/.alfworld alfworld-generate --data_path $ALFWORLD_DATA/json_2.1.1 --save_path $ALFWORLD_DATA/human --goal_desc_human_anns_prob 1
""")
        if load_expert:
            config['general']['training_method'] = 'dagger'
        else:
            config['general']['training_method'] = 'dqn'  # don't use expert plan
        config['rl']['training']['max_nb_steps_per_episode'] = max_training_steps
        config['dagger']['training']['max_nb_steps_per_episode'] = max_training_steps
        config['dataset']['data_path'] = root.joinpath('synth', 'train').as_posix()
        config['dataset']['eval_id_data_path'] = root.joinpath('human', 'valid_seen').as_posix()
        config['dataset']['eval_ood_data_path'] = root.joinpath('human', 'valid_unseen').as_posix()
        return config
