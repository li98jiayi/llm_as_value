import os
import tyro
import torch
import shutil
import random
import ujson as json
import lightning as L
from torch import optim
from pathlib import Path
from datasets import Dataset
from torch.nn import functional as F
from dataclasses import dataclass, asdict
from torch.utils.data import DataLoader
from lightning.pytorch import callbacks as C
from verbalized_envs import VerbalizedALFWorld
from lightning.pytorch.loggers import WandbLogger
from train_ppo import Agent
from argparse import Namespace


def load_splits(root, seed=0, p_dev=0.2, context_len=5):
    root = Path(root)
    data = []
    for fname in root.glob('*.json'):
        with open(fname) as f:
            rollout = json.load(f)
        for t in range(len(rollout['history']) - 1):
            step = rollout['history'][t]
            history = rollout['history'][:t+1]
            render = VerbalizedALFWorld.render_indenpendent(
                task=rollout['task'],
                history=history,
                context_len=context_len,
            )
            data.append(dict(context=render, action=step['act']['taken'], admissible=step['info']['admissible']))
    num_dev = int(len(data) * p_dev)
    random.Random(seed).shuffle(data)
    return dict(train=Dataset.from_list(data[:-num_dev]), dev=Dataset.from_list(data[-num_dev:]))


class PLWrapper(L.LightningModule):

    def __init__(self, agent, total_steps=None):
        super().__init__()
        self.agent = agent
        self.total_steps = total_steps
        self.save_hyperparameters(Namespace(**asdict(agent.args)))  # this makes things accessible in self.hparams

    def validation_step(self, batch, batch_idx):
        return self.training_step(batch, batch_idx, prefix='val')

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx, prefix='test')

    def training_step(self, batch, batch_idx, prefix='train'):
        batch_size = len(batch)
        decoder_last_state, valid = self.agent.forward(text=[ex['context'] for ex in batch], admissible=[ex['admissible'] for ex in batch])
        logits = self.agent.action_scorer(decoder_last_state).squeeze(2)
        logits = logits - 1e20 * (1-valid)

        labels = torch.tensor([ex['admissible'].index(ex['action']) for ex in batch], device=self.device)
        loss = F.cross_entropy(logits, labels)
        self.log('charts/{}_bc_loss'.format(prefix), loss, batch_size=batch_size)
        pred = logits.max(1)[1]
        self.log('charts/{}_bc_acc'.format(prefix), pred.eq(labels).float().mean().item(), batch_size=batch_size)
        return loss

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=self.hparams.learning_rate)
        return optimizer


@dataclass
class Args:
    exp_name: str = os.path.basename(__file__)[: -len(".py")]
    """the name of this experiment"""
    seed: int = 1
    """seed of the experiment"""
    torch_deterministic: bool = True
    """if toggled, `torch.backends.cudnn.deterministic=False`"""
    cuda: bool = True
    """if toggled, cuda will be enabled by default"""
    track: bool = False
    """if toggled, this experiment will be tracked with Weights and Biases"""
    wandb_project_name: str = "r2l-tutorial-planning"
    """the wandb's project name"""
    wandb_entity: str = None
    """the entity (team) of wandb's project"""

    # Algorithm specific arguments
    total_timesteps: int = 1000000
    """total timesteps of the experiments"""
    learning_rate: float = 2.5e-4
    """the learning rate of the optimizer"""
    batch_size: int = 24
    """the batch size of sample from the reply memory"""

    # custom
    split: str = 'train'
    """environment split"""
    alfworld_data: str = os.path.join(os.environ['HOME'], '.alfworld')
    """alfworld data root"""
    base_model_id: str = 'google/flan-t5-small'
    """base LLM to use"""
    max_input_len: int = 512
    """max number of tokens in input"""
    accumulate_grad_batches: int = 1
    """how many batches to accumulate"""
    max_epochs: int = 15
    """max training epochs"""
    log_every_n_steps: int = 1000
    """how often to log"""
    dout: str = 'results/bc/{run_name}'
    """where to save results"""
    dbc_rollout: str = 'data/bc'
    """where BC rollouts are"""


if __name__ == '__main__':
    args = tyro.cli(Args)
    run_name = f"alfworld-{args.split}__{args.exp_name}__{args.seed}"
    L.seed_everything(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    # see if GPU has tensor cores
    try:
        torch.set_float32_matmul_precision('medium')
    except Exception:
        pass
    os.environ['TOKENIZERS_PARALLELISM'] = 'false'

    dout = args.dout.format(run_name=run_name)
    if os.path.isdir(dout):
        print('Removing existing experiment at {}'.format(dout))
        shutil.rmtree(dout)
    os.makedirs(dout)

    data = load_splits(args.dbc_rollout, p_dev=0.2)
    print({k: len(v) for k, v in data.items()})

    train_dataloader = DataLoader(data['train'], batch_size=args.batch_size, shuffle=True, collate_fn=lambda x: x)
    val_dataloader = DataLoader(data['dev'], batch_size=args.batch_size, shuffle=False, collate_fn=lambda x: x)

    model = PLWrapper(Agent(args), total_steps=len(train_dataloader) * args.max_epochs)

    logger = None
    if args.track:
        logger = WandbLogger(project=args.wandb_project_name, entity=args.wandb_entity, name=run_name, save_dir=dout, log_model=False)

    trainer = L.Trainer(
        default_root_dir=dout,
        accumulate_grad_batches=args.accumulate_grad_batches,
        max_epochs=args.max_epochs,
        val_check_interval=0.5,
        log_every_n_steps=args.log_every_n_steps,
        gradient_clip_val=5.0,
        callbacks=[
            C.ModelCheckpoint(
                dirpath=dout,
                filename='{step}-{charts/val_bc_loss:.3f}-{charts/val_bc_acc:.3f}',
                monitor='charts/val_bc_acc',
                mode='max',
                save_top_k=1,
                save_last=True,
                save_on_train_epoch_end=False,
            ),
        ],
        logger=logger,
    )
    trainer.fit(model=model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)
