import os
import json
import torch
import dacite
from tqdm import auto as tqdm
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from verbalized_envs import VerbalizedALFWorld


def evaluate(gamefiles, model, split, max_steps=50, sample=False):
    device = torch.device('cpu')
    if torch.cuda.is_available():
        device = torch.device('cuda')
    model = model.to(device)
    results = {}
    won = []
    for game in (bar := tqdm.tqdm(gamefiles)):
        env = VerbalizedALFWorld(split=split, verbose=False, return_history=True, max_steps=max_steps, force_game=game, shuffle=False)
        obs, info = env.reset()
        done = False
        while not done:
            act = model.act(text=[obs], admissible=[info['admissible']])[0]
            obs, reward, done, truncated, info = env.step(act)
        won.append(info['history'][-1]['reward'])
        bar.set_description('won: {}/{} = {}'.format(sum(won), len(won), sum(won)/len(won)))
        results[game] = info['history']
        env.close()
    return results


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--max_steps', default=80, type=int)
    parser.add_argument('--fsave', default='results/bc/llm-small/last.ckpt')
    parser.add_argument('--split', default='eval_out_of_distribution')
    parser.add_argument('--sample', action='store_true')
    parser.add_argument('--save_pretrained', action='store_true')
    parser.add_argument('--track', help='wandb entity')
    parser.add_argument('--wandb_entity', default='r2llab', help='wandb entity')
    args = parser.parse_args()

    # see if GPU has tensor cores
    try:
        torch.set_float32_matmul_precision('medium')
    except Exception:
        pass
    os.environ['TOKENIZERS_PARALLELISM'] = 'false'

    env = VerbalizedALFWorld(split=args.split, verbose=True, return_history=True, max_steps=args.max_steps)
    gamefiles = env.gamefiles
    gamefiles = ['/'.join(g.split('/')[-3:-1]) for g in gamefiles]
    env.close()

    if 'ppo' in args.fsave:
        from train_ppo import Agent, Args
        state = torch.load(args.fsave)
        model = Agent(state['args'])
        model.load_state_dict(state['agent'])
    elif 'dqn' in args.fsave:
        from train_dqn import Agent, Args
        state = torch.load(args.fsave)
        model = Agent(state['args'])
        model.load_state_dict(state['q_network'], strict=False)
    elif 'bc' in args.fsave:
        from train_ppo import Agent, Args
        # ideally we don't use tyro and dacite, but for learing purpose we try to keep the CleanRL code to be as close to original as possible
        state = torch.load(args.fsave)
        model = Agent(dacite.from_dict(data_class=Args, data=state['hyper_parameters'], config=dacite.Config(check_types=False)))
        partial = {k.replace('agent.', ''): v for k, v in state['state_dict'].items() if k.startswith('agent.')}
        model.load_state_dict(partial)
    else:
        raise NotImplementedError('Unsupported save file {}'.format(args.fsave))
    model.eval()

    if args.track:
        import wandb
        run_name = f"alfworld-{args.split}__{model.args.exp_name}__{model.args.seed}"
        api = wandb.Api()
        runs = list(api.runs(path="{}/{}".format(args.track, model.args.wandb_project_name), filters={"config.exp_name": model.args.exp_name}))
        print('Found {} existing runs'.format(len(runs)))

    results = evaluate(gamefiles, model, args.split, max_steps=args.max_steps, sample=args.sample)
    win = [h[-1]['reward'] for h in results.values()]
    summary = dict(win_rate=sum(win) / len(win))
    print(summary)

    if args.track:
        for run in runs:
            for k, v in summary.items():
                run.summary['eval_{}'.format(k)] = v
                run.summary.update()

    dsave = os.path.dirname(args.fsave)
    prefix = 'eval.{}.{}'.format(args.split, 'sample' if args.sample else 'greedy')
    with open(os.path.join(dsave, '{}.json'.format(prefix)), 'wt') as f:
        json.dump(results, f, indent=2)
    with open(os.path.join(dsave, '{}.summary.json'.format(prefix)), 'wt') as f:
        json.dump(summary, f, indent=2)
