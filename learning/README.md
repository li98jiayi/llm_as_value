# Planning

This directory provides examples for learning how to plan using ALFWorld, a verbalized text environment.
This was not written for modularity.
Instead, we tried to stay as close to [CleanRL](https://docs.cleanrl.dev/) implementations so that RL examples are more self-contained and easier to understand.
The behaviour cloning code was written with RL simplicity in mind.
In other words, you can view `train_bc.py` as a pretraining wrapper for the RL scripts.

```
# Install
pip install -r requirements.txt
python generate_demos.py

# For the commands below, you can add --track to log to wandb

# Behaviour cloning
python train_bc.py
python evaluate_policy.py --fsave results/bc/llm-small/last.ckpt

 # RL from scratch
python train_ppo.py --split eval_out_of_distribution
python train_dqn.py --split eval_out_of_distribution

python train_ppo.py --exp_name pretrained_ppo --split eval_out_of_distribution --fpretrained results/bc/alfworld-train__train_bc__1/last.ckpt --learning_rate 1e-5
python train_dqn.py --exp_name pretrained_dqn --split eval_out_of_distribution --fpretrained results/bc/alfworld-train__train_bc__1/last.ckpt --learning_rate 1e-5 --fill_buffer_on_policy
```


# Notes

- [CleanRL](https://docs.cleanrl.dev/)
- [The 37 Implementation Details of Proximal Policy Optimization](https://iclr-blog-track.github.io/2022/03/25/ppo-implementation-details/)
