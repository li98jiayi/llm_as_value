# docs and experiment results can be found at https://docs.cleanrl.dev/rl-algorithms/dqn/#dqnpy
import os
import random
import time
from dataclasses import dataclass

import gymnasium as gym
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import tyro
from torch.utils.tensorboard import SummaryWriter

import copy
import typing as T
from verbalized_envs import VerbalizedALFWorld
from torch.distributions.categorical import Categorical
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


class ReplayBuffer:

    def __init__(self, buffer_size, num_envs, device, eviction='lru'):
        self.buffer_size = max(1, buffer_size // num_envs)
        self.num_envs = num_envs
        self.device = device
        self.cache = []
        self.eviction = eviction

    def add(self, data):
        self.cache.append(data)
        if len(self.cache) > self.buffer_size:
            if self.eviction == 'lru':
                self.cache.pop(0)
            else:
                raise NotImplementedError('Unsupported eviction policy "{}"'.format(self.eviction))

    def sample(self, batch_size, merge='stack', merge_except=None):
        batch_indices = np.random.randint(0, len(self.cache)-1, size=batch_size).tolist()
        env_indices = np.random.randint(0, high=self.num_envs, size=len(batch_indices)).tolist()
        out = {}
        keys = self.cache[0].keys()
        for k in keys:
            sub = [self.cache[bi][k][ei] for bi, ei in zip(batch_indices, env_indices)]
            if not merge_except or k not in merge_except:
                if merge == 'stack':
                    sub = torch.from_numpy(np.stack(sub, axis=0))
                    if sub.dtype is torch.double:
                        sub = sub.float()
                    sub = sub.to(self.device)
                else:
                    raise NotImplementedError('Unsupported merge "{}"'.format(merge))
            out[k] = sub
        return out


@dataclass
class Args:
    exp_name: str = os.path.basename(__file__)[: -len(".py")]
    """the name of this experiment"""
    seed: int = 1
    """seed of the experiment"""
    torch_deterministic: bool = True
    """if toggled, `torch.backends.cudnn.deterministic=False`"""
    cuda: bool = True
    """if toggled, cuda will be enabled by default"""
    track: bool = False
    """if toggled, this experiment will be tracked with Weights and Biases"""
    wandb_project_name: str = "r2l-tutorial-planning"
    """the wandb's project name"""
    wandb_entity: str = None
    """the entity (team) of wandb's project"""

    # Algorithm specific arguments
    total_timesteps: int = 1000000
    """total timesteps of the experiments"""
    learning_rate: float = 2.5e-4
    """the learning rate of the optimizer"""
    num_envs: int = 1
    """the number of parallel game environments"""
    buffer_size: int = 10000
    """the replay memory buffer size"""
    gamma: float = 0.99
    """the discount factor gamma"""
    tau: float = 1.0
    """the target network update rate"""
    target_network_frequency: int = 500
    """the timesteps it takes to update the target network"""
    batch_size: int = 24
    """the batch size of sample from the reply memory"""
    start_e: float = 1
    """the starting epsilon for exploration"""
    end_e: float = 0.05
    """the ending epsilon for exploration"""
    exploration_fraction: float = 0.5
    """the fraction of `total-timesteps` it takes from start-e to go end-e"""
    learning_starts: int = 10000
    """timestep to start learning"""
    train_frequency: int = 10
    """the frequency of training"""

    # custom
    split: str = 'train'
    """environment split"""
    alfworld_data: str = os.path.join(os.environ['HOME'], '.alfworld')
    """alfworld data root"""
    base_model_id: str = 'google/flan-t5-small'
    """base LLM to use"""
    max_input_len: int = 512
    """max number of tokens in input"""
    num_actions: int = 100
    """max number of actions"""
    progress_bar: bool = False
    """show progress bar"""
    save_every_iterations: int = 100000
    """how many interations to save"""
    dout: str = 'results/dqn'
    """where to save results"""
    num_steps: int = 80
    """how many steps to run the environment"""
    fpretrained: str = None
    """load pretrained BC model"""
    fill_buffer_on_policy: bool = False
    """Use the model to fill the buffer, otherwise use random sampling"""


def make_env(args):

    def thunk():
        return VerbalizedALFWorld(split=args.split, max_steps=args.num_steps, shuffle=True, alfworld_data=args.alfworld_data)

    return thunk


# ALGO LOGIC: initialize agent here:
class Agent(nn.Module):

    def __init__(self, args):
        super().__init__()
        self.args = args
        self.tokenizer = AutoTokenizer.from_pretrained(args.base_model_id)
        self.llm = AutoModelForSeq2SeqLM.from_pretrained(args.base_model_id)
        self.hidden_size = self.llm.config.hidden_size
        # this action_scorer is only used to preload the action scorer from pretrained policies, it is then not used any more
        self.action_scorer = nn.Sequential(
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(),
            nn.Linear(self.hidden_size, 1),
        )
        # this is the actual value head that is used in Q learning
        self.qvalue_scorer = nn.Sequential(
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(),
            nn.Linear(self.hidden_size, 1),
        )

    @property
    def device(self):
        return self.action_scorer[0].weight.device

    def encode(self, text: T.List[T.AnyStr]):
        context = self.tokenizer(text, max_length=self.args.max_input_len, padding=True, truncation=True, add_special_tokens=True, return_tensors='pt').to(self.device)
        encoder_output_vectors = self.llm.base_model.encoder(
            input_ids=context.input_ids, attention_mask=context.attention_mask, return_dict=True
        ).last_hidden_state
        return encoder_output_vectors

    def forward(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]], encoder_output_vectors=None, num_actions=None):
        if encoder_output_vectors is None:
            encoder_output_vectors = self.encode(text)
        admissible = admissible[:num_actions]
        max_admissible = max(len(a) for a in admissible)
        admissible_mask = []
        last_state = []
        for enc, a in zip(encoder_output_vectors, admissible):
            admissible_mask.append([1] * len(a) + [0] * (max_admissible - len(a)))
            a.extend(['nop'] * (max_admissible - len(a)))
            action = self.tokenizer(['{} {}'.format(self.tokenizer.pad_token_id, ai) for ai in a], padding=True, truncation=True, add_special_tokens=False, return_tensors='pt').to(self.device)
            enc = enc.unsqueeze(0).repeat(len(a), 1, 1)
            decoder_output_vectors = self.llm.base_model.decoder(
                action.input_ids, encoder_hidden_states=enc
            ).last_hidden_state
            last_state.append(decoder_output_vectors[:, -1])
        last_state = torch.stack(last_state, dim=0)
        valid = torch.tensor(admissible_mask, device=self.device, dtype=torch.float)
        return last_state, valid

    def get_qvalue(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]]):
        decoder_last_state, valid = self.forward(text, admissible=admissible, num_actions=self.args.num_actions)
        value = self.qvalue_scorer(decoder_last_state).squeeze(2)
        # value = value * valid + 0 * (1-valid)
        return value

    def act(self, text: T.List[T.AnyStr], admissible: T.List[T.List[T.AnyStr]], sample: bool = False, return_raw_ids: bool = False, action_scorer=None):
        decoder_last_state, valid = self.forward(text, admissible=admissible, num_actions=self.args.num_actions)
        if action_scorer is None:
            action_scorer = self.qvalue_scorer
        value = action_scorer(decoder_last_state).squeeze(2)
        ids = value.max(1)[1]
        if sample:
            probs = Categorical(logits=value)
            ids = probs.sample()
        return ids if return_raw_ids else [a[i] for a, i in zip(admissible, ids.tolist())]


def linear_schedule(start_e: float, end_e: float, duration: int, t: int):
    slope = (end_e - start_e) / duration
    return max(slope * t + start_e, end_e)


if __name__ == "__main__":
    args = tyro.cli(Args)
    assert args.num_envs == 1, "vectorized envs are not supported at the moment"
    run_name = f"alfworld-{args.split}__{args.exp_name}__{args.seed}"
    if args.track:
        import wandb

        wandb.init(
            project=args.wandb_project_name,
            entity=args.wandb_entity,
            sync_tensorboard=True,
            config=vars(args),
            name=run_name,
            monitor_gym=True,
            save_code=True,
        )
    dout = f"{args.dout}/{run_name}"
    writer = SummaryWriter(dout)
    writer.add_text(
        "hyperparameters",
        "|param|value|\n|-|-|\n%s" % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )

    # TRY NOT TO MODIFY: seeding
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")

    # env setup
    envs = gym.vector.SyncVectorEnv(
        [make_env(args) for i in range(args.num_envs)],
    )
    # assert isinstance(envs.single_action_space, gym.spaces.Discrete), "only discrete action space is supported"

    q_network = Agent(args).to(device)
    optimizer = optim.Adam(q_network.parameters(), lr=args.learning_rate)
    target_network = Agent(args).to(device)
    target_network.load_state_dict(q_network.state_dict())

    if args.fpretrained:
        state = torch.load(args.fpretrained)
        partial = {k.replace('agent.llm.', ''): v for k, v in state['state_dict'].items() if k.startswith('agent.llm.')}
        q_network.llm.load_state_dict(partial)
        target_network.llm.load_state_dict(partial)
        partial = {k.replace('agent.action_scorer.', ''): v for k, v in state['state_dict'].items() if k.startswith('agent.action_scorer.')}
        q_network.action_scorer.load_state_dict(partial)
        target_network.action_scorer.load_state_dict(partial)

    rb = ReplayBuffer(
        args.buffer_size,
        args.num_envs,
        device,
    )
    start_time = time.time()

    # TRY NOT TO MODIFY: start the game
    obs, infos = envs.reset(seed=args.seed)
    admissibles = infos['admissible']
    num_completions = 0
    for global_step in range(args.total_timesteps):
        if global_step % args.save_every_iterations == 0:
            torch.save(dict(
                q_network=q_network.state_dict(),
                target_network=target_network.state_dict(),
                optimizer=optimizer.state_dict(),
                args=args,
                global_step=global_step,
            ), os.path.join(dout, 'save_iter{}.ckpt'.format(global_step)))

        # ALGO LOGIC: put action logic here
        epsilon = linear_schedule(args.start_e, args.end_e, args.exploration_fraction * args.total_timesteps, global_step)
        if random.random() < epsilon:
            if args.fill_buffer_on_policy:
                with torch.no_grad():
                    action_scorer = q_network.action_scorer if global_step < args.learning_starts else q_network.qvalue_scorer
                    actions = q_network.act(obs, admissibles, sample=True, return_raw_ids=True, action_scorer=action_scorer).cpu().numpy()
            else:
                actions = np.array([random.randint(0, len(a) - 1) for a in admissibles])
        else:
            q_values = q_network.get_qvalue(obs, admissibles)
            actions = torch.argmax(q_values, dim=1).cpu().numpy()
        real_action = [a[i] for i, a in zip(actions.tolist(), admissibles)]

        # TRY NOT TO MODIFY: execute the game and log data.
        next_obs, rewards, terminations, truncations, infos = envs.step(real_action)
        next_admissibles = infos['admissible']

        # TRY NOT TO MODIFY: record rewards for plotting purposes
        if "final_info" in infos:
            for info in infos["final_info"]:
                if info and "episode" in info:
                    print(f"global_step={global_step}, episodic_return={info['episode']['r']}")
                    if global_step > args.learning_starts:
                        num_completions += 1
                        writer.add_scalar("charts/num_completions", num_completions, global_step)
                        writer.add_scalar("charts/episodic_return", info["episode"]["r"], global_step)
                        writer.add_scalar("charts/episodic_length", info["episode"]["l"], global_step)

        # TRY NOT TO MODIFY: save data to reply buffer; handle `final_observation`
        real_next_obs = copy.deepcopy(list(next_obs))
        real_next_admissibles = copy.deepcopy(next_admissibles)
        for idx, trunc in enumerate(truncations):
            if trunc:
                real_next_obs[idx] = infos["final_observation"][idx]
                real_next_admissibles[idx] = infos['final_info'][idx]['admissible']
        rb.add(dict(
            observations=obs,
            admissibles=admissibles,
            next_observations=real_next_obs,
            next_admissibles=next_admissibles,
            actions=actions,
            rewards=rewards,
            dones=terminations,
        ))

        # TRY NOT TO MODIFY: CRUCIAL step easy to overlook
        obs = next_obs
        admissibles = next_admissibles

        # ALGO LOGIC: training.
        if global_step > args.learning_starts:
            if global_step % args.train_frequency == 0:
                data = rb.sample(args.batch_size, merge_except={'observations', 'next_observations', 'admissibles', 'next_admissibles'})
                with torch.no_grad():
                    target_max, _ = target_network.get_qvalue(data['next_observations'], data['next_admissibles']).max(dim=1)
                    td_target = data['rewards'].flatten() + args.gamma * target_max * (1 - data['dones'].float().flatten())
                old_val = q_network.get_qvalue(data['observations'], data['admissibles']).gather(1, data['actions'].unsqueeze(1)).squeeze()
                loss = F.mse_loss(td_target, old_val)

                if global_step % 100 == 0:
                    writer.add_scalar("losses/td_loss", loss, global_step)
                    writer.add_scalar("losses/q_values", old_val.mean().item(), global_step)
                    print("SPS:", int(global_step / (time.time() - start_time)))
                    writer.add_scalar("charts/SPS", int(global_step / (time.time() - start_time)), global_step)

                # optimize the model
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            # update target network
            if global_step % args.target_network_frequency == 0:
                for target_network_param, q_network_param in zip(target_network.parameters(), q_network.parameters()):
                    target_network_param.data.copy_(
                        args.tau * q_network_param.data + (1.0 - args.tau) * target_network_param.data
                    )

    envs.close()
    writer.close()
