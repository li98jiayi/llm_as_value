import os
import tqdm
import random
import logging
import argparse
import ujson as json
import tenacity as T
from pathlib import Path
from openai import AzureOpenAI
from collections import defaultdict


instruction = """
Given the following task, past observations, and actions, first, give an integer score between 1 and 5 indicating how close the player is to solving the task, with 1 being not close and 5 being very close. Next, in a bullet list under section # Summary, describe what you think the player is trying to do.
Finally, in a bullet list under section # Improvement, describe how you think the player can improve their strategy in order to achieve the task more effectively.
"""

system_prompt = """
You are an expert annotator tasked with evaluating how good a player is in playing a game. Your job is to watch the player and comment on their performance.
"""


def verbalize(task, window) -> str:
    text = ['Task: {}'.format(task)]
    c = window[-1]
    for s in window:
        text.append('Time t{time}\nObs:\n{obs}\nAct:\n{act}'.format(
            time=s['time'] - c['time'],
            obs=s['prev'],
            act=s['act'] if s['time'] != c['time'] else '',
        ))
    return '\n\n'.join(text)


@T.retry(stop=T.stop_after_attempt(50), wait=T.wait_fixed(5), after=lambda s: logging.error(repr(s)))
def query(prompt, system_prompt, model='gpt-4'):
    out = client.chat.completions.create(
      model=model,
      messages=[
        {"role": "system", "content": system_prompt},
        {"role": "user", "content": prompt}
      ]
    )
    return out.choices[0].message.content


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dinp', default='data/mixed_policy_rollouts')
    parser.add_argument('--split', default='eval_out_of_distribution')
    parser.add_argument('--dout', default='data/comment')
    parser.add_argument('--history', default=10, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--fsecrets', default='secrets.json')
    parser.add_argument('--num_samples', default=5000, type=int)
    parser.add_argument('--reversed', action='store_true')
    parser.add_argument('--subsample', action='store_true')
    parser.add_argument('--dry', action='store_true')
    args = parser.parse_args()

    dinp = Path(args.dinp).joinpath(args.split)
    dout = Path(args.dout).joinpath(args.split)
    if not dout.exists():
        os.makedirs(dout.as_posix())

    with open('secrets.json') as f:
        secrets = json.load(f)
    client = AzureOpenAI(
        api_key=secrets['OPENAI_API_KEY'],
        api_version="2023-12-01-preview",
        azure_endpoint=secrets['OPENAI_API_BASE'],
    )

    files = list(dinp.glob('*.json'))
    print('Found {} rollouts, selecting {}'.format(len(files), args.num_samples))

    data = []
    for fname in tqdm.tqdm(files, desc='collecting data'):
        result = []
        try:
            with open(fname) as f:
                rollout = json.load(f)
        except Exception:
            print('Cannot process {}'.format(fname))
            continue
        task = rollout['task']
        num_steps = sum(not s.get('planner') for s in rollout['steps'])
        policy = rollout['steps'][:num_steps+1]
        window = policy[-args.history:]
        verb = verbalize(task, window)
        prompt = '{}\n\n{}'.format(instruction.strip(), verb.rstrip()).replace('Act:', 'Action:').replace('Obs:', 'Obersvation:')[:-len('Action:')].strip()
        data.append(dict(prompt=prompt, fname=os.path.basename(fname), t=num_steps, num_planner=len(rollout['steps'])-num_steps))

    rng = random.Random(args.seed)
    rng.shuffle(data)

    if args.subsample:
        data_by_steps = defaultdict(list)
        steps = [3, 5, 7, 10, 15, float('inf')]
        for ex in data:
            last = -1
            group = None
            for step in steps:
                if last < ex['num_planner'] <= step:
                    group = '{} < x < {}'.format(last, step)
                last = step
            data_by_steps[group].append(ex)
        min_size = min(len(v) for v in data_by_steps.values())
        samples = []
        for k, v in data_by_steps.items():
            random.Random(args.seed).shuffle(v)
            samples.extend(v[:min_size])
    else:
        samples = data[:args.num_samples]

    if args.reversed:
        samples = list(reversed(samples))

    print('Querying LLM for {} samples'.format(len(samples)))
    for ex in tqdm.tqdm(samples, desc='querying llm'):
        fout = dout.joinpath(ex['fname'])
        if os.path.isfile(fout):
            continue
        if not args.dry:
            ex['llm'] = query(ex['prompt'], system_prompt)
            with open(fout, 'wt') as f:
                json.dump(ex, f, indent=2)
