set -x
set -e

# for t in score summary improvement score,summary score,improvement summary,improvement score,summary,improvement; do
#   python train_value_func.py --expname $t --feedback_type $t
# done
# for t in score summary improvement score,summary score,improvement summary,improvement score,summary,improvement; do
#   python eval_value_func.py --dcomment data/comment --dsave results/value_llm/$t
# done

for t in score summary improvement score,summary score,improvement summary,improvement score,summary,improvement; do
  python eval_feedback_model.py --batch_size 50 --dsave results/feedback_llm/$t
  python get_lfm_comment.py --dexp results/feedback_llm/$t
  python eval_value_func.py --dcomment results/feedback_llm/$t/pred --dsave results/value_llm/$t --prefix lfm-
done
