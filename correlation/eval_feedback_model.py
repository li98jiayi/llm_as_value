import os
import tqdm
import json
import torch
import lightning as L
from pathlib import Path
from importlib import import_module
from torch.utils.data import DataLoader
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, Namespace
from transformers import AutoTokenizer
from dataset import load_feedback, MyFeedbackDataset


def flatten_preds(predictions):
    preds = []
    for p in predictions:
        preds.extend(p.tolist())
    return preds


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dcomment', default='data/comment')
    parser.add_argument('--drollout', default='data/mixed_policy_rollouts')
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--batch_size', default=20, type=int)
    parser.add_argument('--dsave', default='results/feedback_llm/default')
    parser.add_argument('--split', default='eval_out_of_distribution')
    args = parser.parse_args()

    # see if GPU has tensor cores
    try:
        torch.set_float32_matmul_precision('medium')
    except Exception:
        pass

    L.seed_everything(args.seed)

    state = torch.load(os.path.join(args.dsave, 'last.ckpt'))
    save_args = Namespace(**state['hyper_parameters'])
    tokenizer = AutoTokenizer.from_pretrained(save_args.base_model_id)

    dcomment = Path(args.dcomment)
    drollout = Path(args.drollout)
    data = load_feedback(dcomment.joinpath(args.split), drollout.joinpath(args.split), load_verb=True, feedback_type=save_args.feedback_type)
    data = MyFeedbackDataset(data, tokenizer, save_args.discount, name='eval')
    Model = import_module('models.{}'.format(save_args.model)).Model
    loader = DataLoader(data, batch_size=args.batch_size, shuffle=False, collate_fn=Model.make_batch)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model = Model(save_args, tokenizer=tokenizer, total_steps=0)
    model.load_state_dict(state['state_dict'])
    model = model.to(device)

    pred = []
    for i, batch in tqdm.tqdm(enumerate(iter(loader)), desc='prediction', total=len(loader)):
        batch = {k: v if isinstance(v, list) else v.to(device) for k, v in batch.items()}
        pred.extend(model.predict_step(batch, i))

    res = model.evaluate_predictions(pred, data)
    print(res)

    stitch = [dict(feedback=p, fname=ex['fname']) for ex, p in zip(data, pred)]
    with Path(args.dsave).joinpath('{}.json'.format(args.split)).open('wt') as f:
        json.dump(stitch, f, indent=2)
    with Path(args.dsave).joinpath('{}_res.json'.format(args.split)).open('wt') as f:
        json.dump(res, f, indent=2)
