import os
import json
from pathlib import Path
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def align_preds(preds, dcomment, dout):
    if not dout.exists():
        os.makedirs(dout.as_posix())
    for p in preds:
        fcomment = dcomment.joinpath(p['fname'])
        with fcomment.open() as f:
            comment = json.load(f)
        comment['llm'] = p['feedback']
        with dout.joinpath(p['fname']).open('wt') as f:
            json.dump(comment, f, indent=2)


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--base_model_id', default='google/flan-t5-large')
    parser.add_argument('--dcomment', default='data/comment')
    parser.add_argument('--dexp', default='results/feedback_llm/default')
    parser.add_argument('--split', default='eval_out_of_distribution')
    temp_args, _ = parser.parse_known_args()
    args = parser.parse_args()

    dcomment = Path(args.dcomment)
    with open(os.path.join(args.dexp, '{}.json'.format(args.split))) as f:
        preds = json.load(f)

    dout = Path(args.dexp).joinpath('pred')
    align_preds(preds, dcomment.joinpath(args.split), dout.joinpath(args.split))
