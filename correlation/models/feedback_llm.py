import re
import torch
import evaluate
from .llm import Model as Base


class Model(Base):

    def forward(self, batch):
        return self.llm(input_ids=batch['X'], attention_mask=batch['Xmask'], labels=batch['Y'])

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        out = self.llm.generate(input_ids=batch['X'], attention_mask=batch['Xmask'], max_new_tokens=self.hparams.max_output_len)
        pred = self.tokenizer.batch_decode(out, skip_special_tokens=True, clean_up_tokenization_spaces=True)
        return pred

    def training_step(self, batch, batch_idx, prefix='train'):
        out = self.forward(batch)
        loss = out.loss
        self.log('{}_loss'.format(prefix), loss)
        return loss

    def validation_step(self, batch, batch_idx, prefix='val'):
        self.training_step(batch, batch_idx, prefix=prefix)

    @staticmethod
    def make_batch(examples):
        X = []
        Xmask = []
        Y = []
        fnames = []
        for ex in examples:
            X.append(ex['verb_ids'])
            Xmask.append(ex['verb_mask'])
            Y.append(ex['feedback_ids'])
            fnames.append(ex['fname'])
        X = torch.cat(X, dim=0)
        Xmask = torch.cat(Xmask, dim=0)
        Y = torch.cat(Y, dim=0)
        return dict(X=X, Xmask=Xmask, Y=Y, fname=fnames)

    def evaluate_predictions(self, preds, dataset):
        m = evaluate.load('bleu')
        res = m.compute(predictions=preds, references=[[ex['feedback']] for ex in dataset])
        return res
