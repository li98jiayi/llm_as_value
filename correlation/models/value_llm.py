import torch
import pandas as pd
from .llm import Model as Base
from torch import nn
from torch.nn import functional as F


class Model(Base):

    def __init__(self, args, tokenizer, total_steps=None):
        super().__init__(args, tokenizer, total_steps)
        self.scorer = nn.Sequential(
            nn.Linear(self.llm.config.hidden_size, self.llm.config.hidden_size),
            nn.Tanh(),
            nn.Linear(self.llm.config.hidden_size, 1),
            nn.Sigmoid(),
        )

    def forward(self, batch):
        out = self.llm.base_model.encoder(
            input_ids=batch['X'],
            attention_mask=batch['Xmask'],
            return_dict=True
        ).last_hidden_state
        scores = self.scorer(out[:, 0]).squeeze(-1)
        return scores

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        scores = self.forward(batch)
        return scores

    def training_step(self, batch, batch_idx, prefix='train'):
        scores = self.forward(batch)
        loss = F.mse_loss(scores, batch['Y'])
        self.log('{}_loss'.format(prefix), loss)
        return loss

    def validation_step(self, batch, batch_idx, prefix='val'):
        self.training_step(batch, batch_idx, prefix=prefix)

    @staticmethod
    def make_batch(examples):
        X = []
        Xmask = []
        Y = []
        for ex in examples:
            X.append(ex['ids'])
            Xmask.append(ex['mask'])
            Y.append(ex['value'])
        X = torch.cat(X, dim=0)
        Xmask = torch.cat(Xmask, dim=0)
        Y = torch.tensor(Y, dtype=torch.float)
        return dict(X=X, Xmask=Xmask, Y=Y)

    def evaluate_predictions(self, preds, dataset):
        preds = pd.Series(preds)
        golds = pd.Series([ex['value'] for ex in dataset])
        assert len(preds) == len(golds)
        r = preds.corr(golds)
        s = preds.corr(golds, method='spearman')
        mse = ((preds - golds)**2).mean()
        abs_err = (preds - golds).abs().mean()
        return dict(pearson=r, spearman=s, mse=mse, abs_err=abs_err)
