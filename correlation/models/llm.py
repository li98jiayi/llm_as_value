import argparse
from .base import Model as Base
from torch import optim
from transformers import get_linear_schedule_with_warmup, AutoModelForSeq2SeqLM


class Model(Base):

    @classmethod
    def add_model_specific_args(cls, parent_parser):
        parser = argparse.ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--max_output_len', type=int, default=1000)
        parser.add_argument('--num_warmup_steps', type=int, default=0)
        return parser

    def __init__(self, args, tokenizer, total_steps=None):
        super().__init__(args, tokenizer, total_steps)
        self.llm = AutoModelForSeq2SeqLM.from_pretrained(args.base_model_id)

    def configure_optimizers(self):
        optimizer = optim.AdamW(self.parameters(), lr=self.hparams.learning_rate)
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=self.hparams.num_warmup_steps, num_training_steps=self.total_steps)
        return dict(optimizer=optimizer, lr_scheduler=scheduler)
