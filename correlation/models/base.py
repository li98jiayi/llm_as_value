import os
import json
from torch import optim
import lightning as L
import argparse


class Model(L.LightningModule):

    @classmethod
    def add_model_specific_args(cls, parent_parser):
        parser = argparse.ArgumentParser(parents=[parent_parser], add_help=False)
        return parser

    def __init__(self, args: argparse.Namespace, tokenizer, total_steps=None):
        super().__init__()
        self.tokenizer = tokenizer
        self.total_steps = total_steps
        self.recent_preds = []
        self.dout = args.dout.format(**vars(args))
        self.save_hyperparameters(args)  # this makes things accessible in self.hparams

    def validation_step(self, batch, batch_idx, prefix='val'):
        xs = self.predict_step(batch, batch_idx)
        context = self.tokenizer.batch_decode(batch['X'], skip_special_tokens=True, clean_up_tokenization_spaces=True)
        gold = self.tokenizer.batch_decode(batch['Y'], skip_special_tokens=True, clean_up_tokenization_spaces=True)
        pred = self.tokenizer.batch_decode(xs, skip_special_tokens=True, clean_up_tokenization_spaces=True)
        num_match = 0
        for c, g, p in zip(context, gold, pred):
            m = g == p
            self.recent_preds.append(dict(context=c, pred=p, gold=g, match=m))
            num_match += m
        acc = num_match / len(gold)
        self.log('{}_acc'.format(prefix), acc)

        loss = self.training_step(batch, batch_idx)
        self.log('{}_loss'.format(prefix), loss)

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx, prefix='test')

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        raise NotImplementedError()

    def training_step(self, batch, batch_idx):
        raise NotImplementedError()

    def policy(self, task, window):
        raise NotImplementedError()

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=self.hparams.learning_rate)
        return optimizer

    def on_save_checkpoint(self, checkpoint):
        # we're going to override this function to write predictions to disk.
        fout = os.path.join(self.dout, 'preds.training.json')
        with open(fout, 'wt') as f:
            json.dump(self.recent_preds, f, indent=2)
        self.recent_preds.clear()

    @staticmethod
    def make_batch(examples):
        raise NotImplementedError()
