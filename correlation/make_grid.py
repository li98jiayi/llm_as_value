import glob
import json
import pandas as pd


data = []
for fname in glob.glob('results/feedback_llm/*/*res.json'):
    with open(fname) as f:
        r = json.load(f)
        r['input'] = fname.split('/')[-2]
        r['val'] = r['val']['bleu']
        r['test'] = r['test']['bleu']
    data.append(r)
df = pd.DataFrame(data)

print('Feedback model')
print(df)


data = []
for fname in glob.glob('results/value_llm/*/*res.json'):
    with open(fname) as f:
        r = json.load(f)
    r['input'] = fname.split('/')[-2]
    data.append(r)
df = pd.DataFrame(data)

print('Value function')
print(df)
