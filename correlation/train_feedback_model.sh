set -x
set -e

# for t in score summary improvement score,summary score,improvement summary,improvement score,summary,improvement; do
#   python train_feedback_model.py --expname $t --feedback_type $t
# done
for t in score summary improvement score,summary score,improvement summary,improvement score,summary,improvement; do
  python eval_feedback_model.py --batch_size 50 --dsave results/feedback_llm/$t --split train
  python get_lfm_comment.py --dexp results/feedback_llm/$t --split train
done
