import os
import json
import torch
import shutil
import lightning as L
from importlib import import_module
from torch.utils.data import DataLoader
from lightning.pytorch import callbacks as C
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from transformers import AutoTokenizer
from dataset import MyFeedbackDataset as MyDataset


def flatten_preds(predictions):
    preds = []
    for p in predictions:
        preds.extend(p)
    return preds


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dcomment', default='data/comment')
    parser.add_argument('--drollout', default='data/mixed_policy_rollouts')
    parser.add_argument('--model', default='feedback_llm')
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--max_epochs', default=15, type=int)
    parser.add_argument('--batch_size', default=10, type=int)
    parser.add_argument('--learning_rate', default=1e-4, type=float)
    parser.add_argument('--discount', default=0.9, type=float)
    parser.add_argument('--dout', default='results/{model}/{expname}')
    parser.add_argument('--expname', default='default')
    parser.add_argument('--base_model_id', default='google/flan-t5-large')
    parser.add_argument('--test_only', action='store_true')
    parser.add_argument('--value_buckets', default=1, type=int)
    parser.add_argument('--feedback_type', default='score,summary,improvement')
    temp_args, _ = parser.parse_known_args()
    Model = import_module('models.{}'.format(temp_args.model)).Model
    parser = Model.add_model_specific_args(parser)
    args = parser.parse_args()

    # see if GPU has tensor cores
    try:
        torch.set_float32_matmul_precision('medium')
    except Exception:
        pass

    L.seed_everything(args.seed)

    dout = args.dout.format(**vars(args))
    if os.path.isdir(dout) and not args.test_only:
        print('Removing existing experiment at {}'.format(dout))
        shutil.rmtree(dout)

    tokenizer = AutoTokenizer.from_pretrained(args.base_model_id)
    dataset = MyDataset.load_splits(args.dcomment, args.drollout, tokenizer, discount_factor=args.discount, value_buckets=args.value_buckets, feedback_type=args.feedback_type)
    train_dataloader = DataLoader(dataset['train'], batch_size=args.batch_size, shuffle=True, collate_fn=Model.make_batch)
    val_dataloader = DataLoader(dataset['dev'], batch_size=args.batch_size, shuffle=False, collate_fn=Model.make_batch)

    print('Dataset')
    for k, v in dataset.items():
        print(k, len(v))

    model = Model(args, tokenizer=tokenizer, total_steps=len(train_dataloader) * args.max_epochs)

    trainer = L.Trainer(
        default_root_dir=dout,
        max_epochs=args.max_epochs,
        val_check_interval=1.0,
        log_every_n_steps=100,
        gradient_clip_val=5.0,
        callbacks=[
            C.ModelCheckpoint(
                dirpath=dout,
                filename='{step}-{val_loss:.3f}',
                monitor='val_loss',
                mode='min',
                save_top_k=1,
                save_last=True,
                save_on_train_epoch_end=False,
            ),
        ],
    )

    if not args.test_only:
        trainer.fit(model=model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)
    state = torch.load(os.path.join(dout, 'last.ckpt'))
    model.load_state_dict(state['state_dict'])

    # train_dataloader = DataLoader(dataset['train'], batch_size=args.batch_size, shuffle=False, collate_fn=Model.make_batch)
    test_dataloader = DataLoader(dataset['test'], batch_size=args.batch_size, shuffle=False, collate_fn=Model.make_batch)
    test_pred = flatten_preds(trainer.predict(model=model, dataloaders=test_dataloader))
    # train_pred = flatten_preds(trainer.predict(model=model, dataloaders=train_dataloader))
    val_pred = flatten_preds(trainer.predict(model=model, dataloaders=val_dataloader))

    res = dict(
        # train=model.evaluate_predictions(train_pred, dataset['train']),
        val=model.evaluate_predictions(val_pred, dataset['dev']),
        test=model.evaluate_predictions(test_pred, dataset['test']),
    )
    print(res)
    with open(os.path.join(dout, 'eval.res.json'), 'wt') as f:
        json.dump(res, f, indent=2)
    with open(os.path.join(dout, 'eval.preds.json'), 'wt') as f:
        dev = []
        for ex, p in zip(dataset['dev'], val_pred):
            dev.append(dict(fname=ex['fname'], feedback=ex['feedback'], pred=p))
        test = []
        for ex, p in zip(dataset['test'], test_pred):
            test.append(dict(fname=ex['fname'], feedback=ex['feedback'], pred=p))
        json.dump(dict(dev=dev, test=test), f, indent=2)
