import random
import numpy as np
import ujson as json
from pathlib import Path
from tqdm import auto as tqdm
from torch.utils.data import Dataset


def verbalize(task, window) -> str:
    text = ['Task: {}'.format(task)]
    c = window[-1]
    for s in window:
        text.append('Time t{time}\nObs:\n{obs}\nAct:\n{act}'.format(
            time=s['time'] - c['time'],
            obs=s['prev'],
            act=s['act'] if s != c else '',
        ))
    return '\n\n'.join(text)


def tokenize(raw, tokenizer, max_length=None):
    tok = tokenizer.encode_plus(
        raw,
        return_tensors="pt",
        return_attention_mask=True,
        max_length=max_length or tokenizer.model_max_length,
        padding='max_length',
        truncation=True,
        add_special_tokens=True,
    )
    return tok


def load_feedback(dcomment, drollout, load_verb=False, feedback_type='score,summary,improvement'):
    data = []
    for fcomment in dcomment.glob('*.json'):
        frollout = drollout.joinpath(fcomment.name)
        if not frollout.exists():
            print('Cannot find {}'.format(frollout))
            continue
        with fcomment.open() as f:
            comment = json.load(f)
        with frollout.open() as f:
            rollout = json.load(f)
        num_expert_steps = sum(s['planner'] for s in rollout['steps'])
        outcome = 1 if rollout['steps'][-1]['won'] else 0

        terms = comment['llm'].split('#')
        score = terms[0]
        summary = '' if len(terms) < 2 else terms[1]
        improvement = '' if len(terms) < 3 else terms[2]

        feedback = []
        for f in feedback_type.split(','):
            if f == 'score':
                feedback.append('# Score\n{}'.format(score.strip()))
            elif f == 'summary':
                feedback.append('# Summary\n{}'.format(summary.strip()))
            elif f == 'improvement':
                feedback.append('# Improvement\n{}'.format(improvement.strip()))
            else:
                raise NotImplementedError('Unsupported feedback type "{}"'.format(f))
        ex = dict(feedback='\n'.join(feedback), num_expert_steps=num_expert_steps, outcome=outcome, fname=fcomment.name)
        if load_verb:
            agent = [x for x in rollout['steps'] if not x['planner']]
            if not agent:
                continue
            ex['verb'] = verbalize(task=rollout['task'], window=agent) + agent[-1]['act']
        data.append(ex)
    return data


def load_rollout(dcomment, drollout, history, feedback_type='score,summary,improvement'):
    # Comment: In the collected dataset, the rollout data is a superset of the comment data. 
    instruction = "Given the following task, past observations, and actions, give a score between 0.0 and 1.0 indicating how close the player is to solving the task, with 0.0 being not close and 1.0 being very close."
    if feedback_type != 'none':
        for f in feedback_type.split(','):
            if f == 'score':
                text = 'A reference score, ranging from 1 to 5 is also given by an expert. A higher score means the player is closer to solving the task.'
                instruction  = instruction.strip() + ' ' + text.strip()
            if f == 'summary':
                text = 'An expert also provides a summary of the player\'s progress.'
                instruction  = instruction.strip() + ' ' + text.strip()
            if f == 'improvement':
                text = 'In addition, an expert provides a suggestion on how the player can improve.'
                instruction  = instruction.strip() + ' ' + text.strip()

    data = []
    for fcomment in dcomment.glob('*.json'):
        frollout = drollout.joinpath(fcomment.name)
        fcomment = dcomment.joinpath(fcomment.name)

        if not frollout.exists():
            print('Cannot find rollout for {}'.format(frollout))
            continue
    
        if feedback_type != 'none':
            if not fcomment.exists():
                print('Cannot find comments for {}'.format(fcomment))
                continue
        
            try:
                with open(fcomment) as f:
                    comment = json.load(f)
            except Exception:
                print('Cannot process comments for {}'.format(fcomment))
                continue

        try:
            with open(frollout) as f:
                rollout = json.load(f)
        except Exception:
            print('Cannot process rollout for {}'.format(frollout))
            continue

        num_expert_steps = sum(s['planner'] for s in rollout['steps'])
        outcome = 1 if rollout['steps'][-1]['won'] else 0

        feedback = []
        # get observation
        task = rollout['task']
        num_steps = sum(not s.get('planner') for s in rollout['steps'])
        policy = rollout['steps'][:num_steps+1]
        window = policy[-history:]
        rollout_verb = verbalize(task, window)
        observation = '# Past Observation \n{}'.format(rollout_verb.rstrip()).replace('Act:', 'Action:').replace('Obs:', 'Obersvation:')[:-len('Action:')].strip()
        feedback.append(observation + '\n')

        if feedback_type != 'none':
            # get comments
            terms = comment['llm'].split('#')
            score = terms[0]
            summary = '' if len(terms) < 2 else terms[1]
            improvement = '' if len(terms) < 3 else terms[2]

            for f in feedback_type.split(','):
                if f == 'score':
                    feedback.append('# Score\n{}'.format(score.strip()))
                elif f == 'summary':
                    feedback.append('# Summary\n{}'.format(summary.strip()))
                elif f == 'improvement':
                    feedback.append('# Improvement\n{}'.format(improvement.strip()))
                else:
                    raise NotImplementedError('Unsupported feedback type "{}"'.format(f))
                
        feedback = '\n'.join(feedback)
        prompt = '{}\n\n{}'.format(instruction.strip(), feedback)
        data.append(dict(feedback=prompt, num_expert_steps=num_expert_steps, outcome=outcome, fname=frollout.name))

    return data


class MyValueDataset(Dataset):

    def __init__(self, data, tokenizer, discount_factor, name='dataset'):
        self.tokenizer = tokenizer
        self.discount_factor = discount_factor
        self.examples = []
        for ex in tqdm.tqdm(data, desc='tokenizing {}'.format(name)):
            ex = ex.copy()
            tok = tokenize(ex['feedback'], self.tokenizer)
            ex['ids'] = tok['input_ids']
            ex['mask'] = tok['attention_mask']
            ex['value'] = ex['outcome'] * self.discount_factor ** ex['num_expert_steps']
            self.examples.append(ex)

    def subsample_by_value_buckets_(self, value_buckets, seed=0):
        rng = random.Random(seed)
        all_values = sorted(list(ex['value'] for ex in self.examples))
        value_linspace = np.linspace(min(all_values), max(all_values), value_buckets+1)

        buckets = []
        min_bucket_size = float('inf')
        print('Buckets:')
        for i in range(len(value_linspace)-1):
            min_value = value_linspace[i]
            max_value = value_linspace[i+1]
            bucket = [ex for ex in self.examples if min_value <= ex['value'] <= max_value]
            print('min', min_value, 'max', max_value, 'support', len(bucket))
            buckets.append(bucket)
            min_bucket_size = min(min_bucket_size, len(bucket))

        self.examples.clear()
        for bucket in buckets:
            rng.shuffle(bucket)
            self.examples.extend(bucket[:min_bucket_size])

    @classmethod
    def load_splits(cls, dcomment, drollout, tokenizer, seed=0, pdev=0.2, discount_factor=0.9, value_buckets=1, history=10, rollout_only=False, feedback_type='score,summary,improvement'):
        dcomment = Path(dcomment)
        drollout = Path(drollout)
        if rollout_only:
            data = load_rollout(dcomment.joinpath('train'), drollout.joinpath('train'), history, feedback_type=feedback_type)
        else:
            data = load_feedback(dcomment.joinpath('train'), drollout.joinpath('train'), load_verb=False, feedback_type=feedback_type)
        random.Random(seed).shuffle(data)
        num_dev = int(len(data) * pdev)
        train = data[:-num_dev]
        dev = data[-num_dev:]
        if rollout_only:
            test = load_rollout(dcomment.joinpath('eval_out_of_distribution'), drollout.joinpath('eval_out_of_distribution'), history, feedback_type=feedback_type)
        else:
            test = load_feedback(dcomment.joinpath('eval_out_of_distribution'), drollout.joinpath('eval_out_of_distribution'), load_verb=False, feedback_type=feedback_type)
        ret = dict(
            train=cls(train, tokenizer, discount_factor, name='train'),
            dev=cls(dev, tokenizer, discount_factor, name='dev'),
            test=cls(test, tokenizer, discount_factor, name='test'),
        )
        if value_buckets > 1:
            for k in ['train', 'dev']:
                ret[k].subsample_by_value_buckets_(value_buckets, seed=seed)
        return ret

    def __getitem__(self, idx):
        return self.examples[idx]

    def __len__(self):
        return len(self.examples)


class MyFeedbackDataset(Dataset):

    def __init__(self, data, tokenizer, discount_factor, name='dataset'):
        self.tokenizer = tokenizer
        self.discount_factor = discount_factor
        self.examples = []
        for ex in tqdm.tqdm(data, desc='tokenizing {}'.format(name)):
            ex = ex.copy()
            tok = tokenize(ex['feedback'], self.tokenizer)
            ex['feedback_ids'] = tok['input_ids']
            ex['feedback_mask'] = tok['attention_mask']
            tok = tokenize(ex['verb'], self.tokenizer)
            ex['verb_ids'] = tok['input_ids']
            ex['verb_mask'] = tok['attention_mask']
            ex['value'] = ex['outcome'] * self.discount_factor ** ex['num_expert_steps']
            self.examples.append(ex)

    @classmethod
    def load_splits(cls, dcomment, drollout, tokenizer, seed=0, pdev=0.2, discount_factor=0.9, value_buckets=1, feedback_type='score,summary,improvement'):
        dcomment = Path(dcomment)
        drollout = Path(drollout)
        data = load_feedback(dcomment.joinpath('train'), drollout.joinpath('train'), load_verb=True, feedback_type=feedback_type)
        random.Random(seed).shuffle(data)
        num_dev = int(len(data) * pdev)
        train = data[:-num_dev]
        dev = data[-num_dev:]
        test = load_feedback(dcomment.joinpath('eval_out_of_distribution'), drollout.joinpath('eval_out_of_distribution'), load_verb=True, feedback_type=feedback_type)
        ret = dict(
            train=cls(train, tokenizer, discount_factor, name='train'),
            dev=cls(dev, tokenizer, discount_factor, name='dev'),
            test=cls(test, tokenizer, discount_factor, name='test'),
        )
        if value_buckets > 1:
            for k in ['train', 'dev']:
                ret[k].subsample_by_value_buckets_(value_buckets, seed=seed)
        return ret

    def __getitem__(self, idx):
        return self.examples[idx]

    def __len__(self):
        return len(self.examples)
