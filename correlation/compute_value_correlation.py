import os
import re
import glob
import argparse
import numpy as np
import pandas as pd
import ujson as json
from tqdm import auto as tqdm


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dcomment', default='data/comment')
    parser.add_argument('--drollout', default='data/mixed_policy_rollouts')
    parser.add_argument('--split', default='eval_out_of_distribution')
    parser.add_argument('--manual_discount', action='store_true')
    parser.add_argument('--discount', default=0.9, type=float)
    args = parser.parse_args()

    data = []
    for fcomment in glob.glob(os.path.join(args.dcomment, args.split, '*.json')):
        fname = os.path.basename(fcomment)
        frollout = os.path.join(args.drollout, args.split, fname)
        with open(fcomment) as f:
            comment = json.load(f)
        with open(frollout) as f:
            rollout = json.load(f)
        e = sum(s['planner'] for s in rollout['steps'])
        o = 1 if rollout['steps'][-1]['won'] else 0
        match = re.findall(r'\d+', comment['llm'])
        llm = float(match[0] if match else 0)
        data.append(dict(
            llm=llm,
            num_expert_steps=e,
            naive_value=0.9**e * o,
            outcome=o,
            policy=rollout['args']['prob_expert'],
        ))
    data_df = pd.DataFrame(data)

    policies = sorted(list(data_df.policy.unique()))
    policy_df = []
    for p in policies:
        df = data_df[data_df.policy == p]
        corr = []
        for t in tqdm.tqdm(np.linspace(0, 1, 101)):
            value = t ** df.num_expert_steps * df.outcome
            corr.append(dict(
                policy=p,
                support=len(df),
                discount=t,
                pearson=value.corr(df.llm),
                spearman=value.corr(df.llm, method='spearman'),
            ))
        corr_df = pd.DataFrame(corr)
        max_i = corr_df.pearson.argmax()
        r = corr_df.iloc[max_i]
        policy_df.append(r.to_dict())
    policy_df = pd.DataFrame(policy_df)
    print(policy_df)

    corr = []
    if not args.manual_discount:
        discounts = tqdm.tqdm(np.linspace(0, 1, 101))
    else:
        discounts = [args.discount]
    for t in discounts:
        values = t ** data_df.num_expert_steps * data_df.outcome
        value_bucket = values * 10 // 2 / 10 * 2
        value_steps = sorted(list(value_bucket.unique()))
        for value in value_steps:
            match = value_bucket == value
            pearson = values[match].corr(data_df[match].llm)
            spearman = values[match].corr(data_df[match].llm, method='spearman')
            corr.append(dict(
                value=value,
                support=sum(match),
                discount=t,
                pearson=pearson,
                spearman=spearman,
            ))
    corr_df = pd.DataFrame(corr)
    value_df = []
    for value in corr_df.value.unique():
        df = corr_df[corr_df.value == value]
        max_i = df.pearson.argmax()
        r = df.iloc[max_i]
        value_df.append(r.to_dict())
    value_df = pd.DataFrame(value_df)
    print(value_df.sort_values(by='value'))
