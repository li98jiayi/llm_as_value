import os
import re
import ray
import tqdm
import json
import random
import argparse
import textworld
import contextlib
from pathlib import Path
from ray.util.actor_pool import ActorPool
from alfworld.agents.environment import AlfredTWEnv
from alfworld.agents.environment.alfred_tw_env import AlfredDemangler, AlfredInfos, AlfredExpert


def patch_config(c, game=None):
    """This is a hack to make ALFWorld load only the specified gamefile"""
    root = Path(os.path.join(os.environ['HOME'], '.alfworld'))
    os.environ['ALFWORLD_DATA'] = root.as_posix()
    # c['general']['training_method'] = 'dqn'  # don't use expert plan
    c['dataset']['data_path'] = root.joinpath('synth', 'train').as_posix()
    c['dataset']['eval_id_data_path'] = root.joinpath('human', 'valid_seen').as_posix()
    c['dataset']['eval_ood_data_path'] = root.joinpath('human', 'valid_unseen').as_posix()

    if game is not None:
        for k in ['data_path', 'eval_id_data_path', 'eval_ood_data_path']:
            c['dataset'][k] = os.path.join(c['dataset'][k], game)
    return c


class AlfredTWEnvWithExpert(AlfredTWEnv):
    """This is a hack to make Alfworld always give us planner output, even for evaluation envs"""

    def init_env(self, batch_size):
        # copied from AlfredTWEnv, only overwriting to add expert for eval set.
        # domain_randomization = self.config["env"]["domain_randomization"]
        # if self.train_eval != "train":
        #     domain_randomization = False
        domain_randomization = False

        alfred_demangler = AlfredDemangler(shuffle=domain_randomization)
        wrappers = [alfred_demangler, AlfredInfos]

        # Register a new Gym environment.
        request_infos = textworld.EnvInfos(won=True, admissible_commands=True, extras=["gamefile"])
        expert_type = self.config["env"]["expert_type"]
        training_method = self.config["general"]["training_method"]

        if training_method == "dqn":
            # max_nb_steps_per_episode = self.config["rl"]["training"]["max_nb_steps_per_episode"]
            max_nb_steps_per_episode = 150
        elif training_method == "dagger":
            # max_nb_steps_per_episode = self.config["dagger"]["training"]["max_nb_steps_per_episode"]
            max_nb_steps_per_episode = 150

            expert_plan = True
            if expert_plan:
                wrappers.append(AlfredExpert(expert_type))
                request_infos.extras.append("expert_plan")

        else:
            raise NotImplementedError

        env_id = textworld.gym.register_games(self.game_files, request_infos,
                                              batch_size=batch_size,
                                              asynchronous=True,
                                              max_episode_steps=max_nb_steps_per_episode,
                                              wrappers=wrappers)
        # Launch Gym environment.
        env = textworld.gym.make(env_id)
        return env


@ray.remote
class Actor:

    def rollout(self, args):
        rng = random.Random(args['seed'])
        config = args['config']
        patch_config(config, game=os.path.dirname(args['gamefile']))
        # get rid of stdout printing
        with open(os.devnull, 'w') as devnull:
            with contextlib.redirect_stdout(devnull):
                with contextlib.redirect_stderr(devnull):
                    env = AlfredTWEnvWithExpert(config, train_eval=args['split'])
                    env = env.init_env(batch_size=1)

        initial_obs, infos = env.reset()
        terms = initial_obs[0].split('\n\n')
        initial_obs = '\n\n'.join(terms[:-1])
        task_str = terms[-1]
        task = re.findall(r'Your task is to: (.+)', task_str)[0].replace('. .', '.')
        env_id = '__'.join(infos['extra.gamefile'][0].split('/')[-3:-1])
        assert env_id == args['env_id']

        traj = dict(env_id=env_id, initial_obs=initial_obs, task=task, num_rollout_steps=0, args={k: v for k, v in args.items() if k != 'config'})
        steps = traj['steps'] = []
        prev_obs = initial_obs
        done = False
        steps.append(dict(time=0, prev=prev_obs, done=None, won=None, admissible=infos['admissible_commands'][0]))

        # roll out mixed policy for fixed steps
        try:
            while not done:
                t = steps[-1]['time']
                if len(steps) < args['num_policy_steps']:
                    # use mixed policy
                    planner = False
                    if rng.uniform(0, 1) < args['prob_expert']:
                        act = infos['extra.expert_plan'][0][0]
                    else:
                        act = rng.choice(infos['admissible_commands'][0])
                    traj['num_rollout_steps'] += 1
                else:
                    # use expert policy
                    planner = True
                    act = infos['extra.expert_plan'][0][0]

                steps[-1]['act'] = act
                steps[-1]['planner'] = planner

                obs, scores, dones, infos = env.step([act])
                steps[-1]['done'] = done = dones[0]
                steps[-1]['won'] = infos['won'][0]

                if not done:
                    steps.append(dict(time=t+1, prev=obs[0], act='', done=None, won=None, admissible=infos['admissible_commands'][0]))
        except Exception as e:
            print(e)
            env.close()
            return

        fout = Path(args['fout'])
        with fout.open('wt') as f:
            json.dump(traj, f, indent=2)
        env.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--fsave', default='results/llm/default/last.ckpt')
    parser.add_argument('--fconfig', default='configs.json')
    parser.add_argument('--split', default='eval_out_of_distribution')
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--max_num_policy_steps', default=50, type=int)
    parser.add_argument('--num_procs', default=1, type=int)
    parser.add_argument('--num_rollouts_per_env', default=50, type=int)
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--dout', default='data/mixed_policy_rollouts/{split}')
    parser.add_argument('--tmpdir', default=os.path.abspath(os.path.join(os.getcwd(), 'tmp')))
    parser.add_argument('--alfworld_data', default=os.path.join(os.environ['HOME'], '.alfworld'))
    args = parser.parse_args()

    random.seed(args.seed)
    with Path(args.fconfig).open('rt') as f:
        config = json.load(f)
        config = config['eval' if args.split != 'train' else 'train']
    os.environ['ALFWORLD_DATA'] = args.alfworld_data

    dout = args.dout.format(split=args.split)
    if not os.path.isdir(dout):
        os.makedirs(dout)

    # generate rollouts
    prob_expert = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
    if args.split == 'train':
        prob_expert = [0.0]

    # get a list of games
    patch_config(config)
    env = AlfredTWEnvWithExpert(config, train_eval=args.split)
    env = env.init_env(batch_size=1)
    gamefiles = env.gamefiles
    env.close()

    # generate job descriptions for parallelization
    rng = random.Random(args.seed)
    sample_configs = []
    for gamefile in gamefiles:
        for i in range(args.num_rollouts_per_env):
            c = dict(
                idx=i,
                gamefile=gamefile,
                config=config.copy(),
                num_policy_steps=rng.randint(0, args.max_num_policy_steps),
                prob_expert=rng.choice(prob_expert),
                split=args.split,
                seed=rng.randint(0, 99999),
                env_id='__'.join(gamefile.split('/')[-3:-1]),
            )
            c['fout'] = fout = os.path.join(dout, 'seed{}idx{}_{}.json'.format(c['seed'], c['idx'], c['env_id']))
            fout = Path(fout)
            if fout.exists() and not args.regenerate:
                try:
                    with fout.open():
                        pass
                except Exception:
                    pass
                else:
                    continue
            sample_configs.append(c)

    kwargs = dict(_temp_dir=args.tmpdir, log_to_driver=False)
    if args.num_procs > 1:
        ray.init(num_cpus=args.num_procs, **kwargs)
    else:
        ray.init(local_mode=True, **kwargs)

    pool = ActorPool([Actor.remote() for _ in range(args.num_procs)])
    for _ in tqdm.tqdm(pool.map_unordered(lambda actor, v: actor.rollout.remote(v), sample_configs), desc='rollout', total=len(sample_configs)):
        # make a progress bar from iterator
        pass
