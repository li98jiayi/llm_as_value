import os
import ray
import tqdm
import json
import random
import argparse
from pathlib import Path
from ray.util.actor_pool import ActorPool
from generate_rollouts import patch_config, AlfredTWEnvWithExpert, Actor


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--fsave', default='results/llm/default/last.ckpt')
    parser.add_argument('--fconfig', default='configs.json')
    parser.add_argument('--split', default='eval_out_of_distribution')
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--num_procs', default=1, type=int)
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--max_num_policy_steps', default=50, type=int)
    parser.add_argument('--num_rollouts_per_env', default=50, type=int)
    parser.add_argument('--dout', default='data/demo_rollouts/{split}')
    parser.add_argument('--tmpdir', default=os.path.abspath(os.path.join(os.getcwd(), 'tmp')))
    parser.add_argument('--alfworld_data', default=os.path.join(os.environ['HOME'], '.alfworld'))
    args = parser.parse_args()

    random.seed(args.seed)
    with Path(args.fconfig).open('rt') as f:
        config = json.load(f)
        config = config['eval' if args.split != 'train' else 'train']
    os.environ['ALFWORLD_DATA'] = args.alfworld_data

    dout = args.dout.format(split=args.split)
    if not os.path.isdir(dout):
        os.makedirs(dout)

    # get a list of games
    patch_config(config)
    env = AlfredTWEnvWithExpert(config, train_eval=args.split)
    env = env.init_env(batch_size=1)
    gamefiles = env.gamefiles
    env.close()

    # generate job descriptions for parallelization
    rng = random.Random(args.seed)
    sample_configs = []
    for gamefile in gamefiles:
        for i in range(args.num_rollouts_per_env):
            c = dict(
                idx=i,
                gamefile=gamefile,
                config=config.copy(),
                split=args.split,
                env_id='__'.join(gamefile.split('/')[-3:-1]),
                num_policy_steps=rng.randint(0, args.max_num_policy_steps),
                seed=rng.randint(0, 99999),
                prob_expert=1.0,
            )
            c['fout'] = fout = os.path.join(dout, 'seed{}idx{}_{}.json'.format(c['seed'], c['idx'], c['env_id']))
            fout = Path(fout)
            if fout.exists() and not args.regenerate:
                continue
            sample_configs.append(c)

    kwargs = dict(_temp_dir=args.tmpdir, log_to_driver=False)
    if args.num_procs > 1:
        ray.init(num_cpus=args.num_procs, **kwargs)
    else:
        ray.init(local_mode=True, **kwargs)

    pool = ActorPool([Actor.remote() for _ in range(args.num_procs)])
    for _ in tqdm.tqdm(pool.map_unordered(lambda actor, v: actor.rollout.remote(v), sample_configs), desc='rollout', total=len(sample_configs)):
        # make a progress bar from iterator
        pass
