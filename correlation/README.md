# Setup

```
pip install -r requirements.txt
env ALWORLD_DATA=$HOME/.alfworld alfworld-download
```

## Generate rollouts

```
python generate_rollouts.py --num_procs 24 --num_rollouts_per_env 100
python generate_rollouts.py --num_procs 24 --split train --num_rollouts_per_env 10  # you might want to use less procs if you don't have a powerful machine
```

## Train models

```
python train_feedback_model.py --batch_size 10 --expname without_score
python train_feedback_model.py --batch_size 10 --expname with_score --keep_llm_score

python train_value_func.py --batch_size 10 --expname without_score
python train_value_func.py --batch_size 10 --expname with_score --keep_llm_score
```

## Evaluate models

```
# generate feedback
python eval_feedback_model.py --batch_size 50 --dsave results/feedback_llm/without_score  # also generates feedback
python eval_feedback_model.py --batch_size 50 --dsave results/feedback_llm/with_score

# extract predictions to a dataset file
python get_lfm_comment.py --dexp results/feedback_llm/with_score
python get_lfm_comment.py --dexp results/feedback_llm/without_score

# evaluate value functions
python eval_value_func.py --dcomment data/comment  # evaluate using LLM feedback
python eval_value_func.py --dcomment results/feedback_llm/without_score/pred  # evaluate using LFM feedback
python eval_value_func.py --dcomment results/feedback_llm/with_score/pred
```
